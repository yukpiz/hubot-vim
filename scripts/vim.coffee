cronJob = require("cron").CronJob

ROOMID = "145577_bot-test@conf.hipchat.com"
module.exports = (robot) ->
    new cronJob("0 * * * *", =>
        new robot.Response(robot, {room: ROOMID}).send "(ﾉ)・ω・(ヾ)ﾋﾞﾑﾋﾞﾑ"
    ).start()
